<x-app-layout>

    <x-slot name="header">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">Create Alert</h2>
            </div>
        </div>
    </x-slot>
    <div class="row">
        <div class="col-sm-4">
            <form action="/alerts/store" method="POST">
                @csrf
                <div class="form-group">
                    <label for="type">Coin</label>
                    <select id="selected_currency" name="currency" class="form-control" id="type">
                        @foreach($currencies as $currency)
                            <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="usr">Value:</label>
                    <input id="value" name="price" type="text" class="form-control" id="value">
                </div>
                <div class="form-group">
                    <label for="type">Movement Type</label>
                    <select name="type" class="form-control" id="type">
                        <option value="1">Above</option>
                        <option value="0">Below</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <img id="coin-image" width="100" height="100">
                        <div id="name" class="col-sm-6"></div>
                        <div id="price" class="col-sm-6"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var json_data = {};
    </script>
    @push('scripts')
        <script src="/js/alerts\create.js"></script>
    @endpush
</x-app-layout>