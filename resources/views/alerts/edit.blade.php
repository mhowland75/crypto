<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">Create Alert</h2>
            </div>
        </div>
    </x-slot>
    <form action="/alerts/store" method="POST">
        @csrf
        <div class="form-group">
            <label for="type">Movement Type</label>
            <select name="currency" class="form-control" id="type">
                @foreach($currencies as $currency)
                    <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="usr">Value:</label>
            <input name="price" type="text" class="form-control" id="value">
        </div>
        <div class="form-group">
            <label for="type">Movement Type</label>
            <select name="type" class="form-control" id="type">
                <option value="1">Above</option>
                <option value="0">Below</option>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</x-app-layout>