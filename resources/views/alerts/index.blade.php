<x-app-layout>
    <x-slot name="header">
    <div class="row">
        <div class="col-sm-11">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">Alerts</h2>
        </div>
        <div class="col-sm-1">
            <a href="/alerts/create">
                <ion-icon name="add-outline"></ion-icon>
            </a>
        </div>
    </div>

    </x-slot>
    <table style="width:100%">
        <tr>
            <th>Name</th>
            <th>Value</th>
            <th>Type</th>
            <th></th>
        </tr>
        @foreach($alerts as $alert)
            <tr>
                <th>{{ $alert->currency->name }}</th>
                <th>{{ $alert->value }}</th>
                @if ($alert->type)
                    <th><ion-icon name="arrow-up-outline"></ion-icon></th>
                @else
                    <th><ion-icon name="arrow-down-outline"></ion-icon></th>
                @endif
                <th>
                    <a href="/alerts/edit/{{ $alert->id }}">
                        <ion-icon name="create-outline"></ion-icon>
                    </a>
                </th>
            </tr>
        @endforeach
    </table>
</x-app-layout>