<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('Currencies') }}</h2>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <form class="form-inline form-control-sm" action="/">
                        <label for="sortby">Sort:</label>
                        <select class="form-control w-25" name="sortby" id="sortby">
                            <option id="Top">Top</option>
                            <option id="Gainers">Gainers</option>
                            <option id="Losers">Losers</option>
                        </select>
                    <label for="time_per">Time:</label>
                    <input type="text" name="time" class="form-control"value="{{ $sortby['time'] }}" id="time_per">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </x-slot>
    <div class="page-header">
        <h1>Currencies</h1>
    </div>
    <div class="row">
            @foreach($data as $currency)
                @if(!$currency['hidden'])
                    <div style="padding-top:20px;" class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <div style="height:300px;" id={{ $currency['acronym'] }}>
                                    <div style="padding-top:25%; padding-left:50%">
                                        <div class="spinner-border"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="box-footer">
                                <div id="box-name-row" class="row">
                                    <div id="box-name" class="col-sm-5">
                                        <a href="/currencies/{{ $currency['acronym'] }}">
                                            {{ $currency['currency']->name }}
                                        </a>
                                    </div>
                                    <div id="box-name" class="col-sm-2">
                                        <img id="currency_logo" src={{ $currency['currency']->logo_url }}>
                                    </div>
                                    
                                    <div id="box-acronym" class="col-sm-5">
                                        <a href="/currencies/{{ $currency['acronym'] }}">{{ $currency['acronym'] }}</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="price-{{$currency['currency']['id']}}" class="col-sm-6 price">£{{ $currency['price'] }}</div>
                                    <div id="perdiff-{{$currency['currency']['id']}}" class="col-sm-6 percentage">{{ $currency['percentage_diff'] }}%</div>            
                                </div>
                                <div id="box-stats-row" class="row">
                                        <div id="stats-lable" class="col-sm-4">Max</div>
                                        <div id="stats-lable" class="col-sm-4">Mean</div>
                                        <div id="stats-lable" class="col-sm-4">Min</div>
                                </div>
                                <div id="box-stats-row" class="row">
                                        <div id="max-{{$currency['currency']['id']}}" class="col-sm-4 avg">£{{ $currency['max_value'] }}</div>
                                        <div id="mean-{{$currency['currency']['id']}}" class="col-sm-4 avg">£{{ $currency['mean_value'] }}</div>
                                        <div id="min-{{$currency['currency']['id']}}" class="col-sm-4 avg">£{{ $currency['min_value'] }}</div>
                                </div>
                            </div>
                    </div>
                @endif
            @endforeach
    </div>

    <script>
        var json_data = {!! $json_data !!};
    </script>
    @push('scripts')
        <script src="/js/currencies\index.js"></script>
    @endpush

</x-app-layout>