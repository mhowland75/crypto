<x-app-layout>
    <x-slot name="header">
    <div class="grid grid-cols-2">
        <div class="col-span-1">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('Currencies') }}</h2>
        </div>
        <div class="col-span-1 justify-self-end">
            <form action="/">
                <select class="w-20 bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-yellow-500 focus:bg-transparent focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" name="sortby" id="sortby">
                    <option id="Top">Top</option>
                    <option id="Gainers">Gainers</option>
                    <option id="Losers">Losers</option>
                </select>
                <input type="text" name="time" class="w-14 bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-yellow-500 focus:bg-transparent focus:ring-2 focus:ring-yellow-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" value="{{ $sortby['time'] }}" id="time_per">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    </x-slot>

    <div class="grid grid-cols-2 gap-4">
        @foreach($data as $currency)
            @if(!$currency['hidden'])
                <div class="col-span-1 font-bold text-xl bg-white rounded border border-gray-600">
                    <div class="col-span-1 h-96 p-2" id={{ $currency['acronym'] }}>
                        <div style="padding-left:47%; padding-top:25%">
                            <div class="spinner-border"></div>
                        </div>
                    </div>
                    <div class="grid grid-cols-12 gap-1 text-gray-600">   
                        <div id="box-name" class="col-span-5 justify-self-end p-0.5">
                            <a class="text-gray-600" href="/currencies/{{ $currency['acronym'] }}">
                                {{ $currency['currency']->name }}
                            </a>
                        </div>
                        <div class="col-span-2 justify-self-center p-0.5">
                            <img id="currency_logo" class="h-6" src={{ $currency['currency']->logo_url }}>
                        </div>
                        <div id="box-acronym" class="col-span-5 p-0.5">
                            <a class="text-gray-600" href="/currencies/{{ $currency['acronym'] }}">
                                {{ $currency['acronym'] }}
                            </a>
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-4 font-bold text-3xl bg-gray-50 ">
                        <div class="col-span-1 justify-self-end p-0.5">£{{ $currency['price'] }}</div>
                        <div class="col-span-1 p-0.5">{{ $currency['percentage_diff'] }}%</div>
                    </div>
                    <div class="grid grid-cols-3 gap-4 justify-items-center text-sm p-0.5">   
                        <div id="stats-lable" class="col-span-1">Max</div>
                        <div id="stats-lable" class="col-span-1">Mean</div>
                        <div id="stats-lable" class="col-span-1">Min</div>
                    </div>
                    <div class="grid grid-cols-3 gap-4 justify-items-center p-0.5">   
                        <div class="col-span-1">£{{ $currency['max_value'] }}</div>
                        <div class="col-span-1">£{{ $currency['mean_value'] }}</div>
                        <div class="col-span-1">£{{ $currency['min_value'] }}</div>
                    </div>
                </div>
             @endif
        @endforeach
    </div>

    <div class="container px-1 py-1 mx-auto w-100">
        <div class="flex flex-wrap -m-0">
            @foreach($data as $currency)
                @if(!$currency['hidden'])
                    <div class="p-2 md:w-1/3 sm:mb-0 mb-8">
                        <div class="rounded-lg  h-64 overflow-hidden">
                            <div style="height:300px;" id={{ $currency['acronym'] }}>
                                <div style="padding-top:25%; padding-left:50%">
                                    <div class="spinner-border"></div>
                                </div>
                            </div>
                        </div>
                        <div id="box-footer">
                            <div id="box-name-row" class="row">
                                <div id="box-name" class="col-sm-5">
                                    <a href="/currencies/{{ $currency['acronym'] }}">
                                        {{ $currency['currency']->name }}
                                    </a>
                                </div>
                                <div id="box-name" class="col-sm-2">
                                    <img id="currency_logo" src={{ $currency['currency']->logo_url }}>
                                </div>
                                
                                <div id="box-acronym" class="col-sm-5">
                                    <a href="/currencies/{{ $currency['acronym'] }}">{{ $currency['acronym'] }}</a>
                                </div>
                            </div>
                            <div class="row">
                                <div id="price-{{$currency['currency']['id']}}" class="col-sm-6 price">£{{ $currency['price'] }}</div>
                                <div id="perdiff-{{$currency['currency']['id']}}" class="col-sm-6 percentage">{{ $currency['percentage_diff'] }}%</div>            
                            </div>
                            <div id="box-stats-row" class="row">
                                    <div id="stats-lable" class="col-sm-4">Max</div>
                                    <div id="stats-lable" class="col-sm-4">Mean</div>
                                    <div id="stats-lable" class="col-sm-4">Min</div>
                            </div>
                            <div id="box-stats-row" class="row">
                                    <div id="max-{{$currency['currency']['id']}}" class="col-sm-4 avg">£{{ $currency['max_value'] }}</div>
                                    <div id="mean-{{$currency['currency']['id']}}" class="col-sm-4 avg">£{{ $currency['mean_value'] }}</div>
                                    <div id="min-{{$currency['currency']['id']}}" class="col-sm-4 avg">£{{ $currency['min_value'] }}</div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>


    <script>
        var json_data = {!! $json_data !!};
    </script>
    @push('scripts')
        <script src="/js/currencies\index.js"></script>
    @endpush

</x-app-layout>