<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{ $data['currency']->name }}</h2>
            </div>
            <div class="col-sm-6">
                <form class="form-inline float-right" action="/">
                    <label for="time_per">Time:</label>
                    <input type="text" name="time" class="form-control" placeholder="{{ $time_per }}" id="time_per">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </x-slot>
    <div class="row">
        <div class="col-sm-9">
        <div class="card">
            <div class="card-body">
                <div style="height:500px;" id="graph" ></div>
            </div>
        </div>
        </div>
        <div class="col-sm-3">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6>Current Price</h6>
                                </div>
                                <div class="col-sm-6">
                                    £{{ $data['price'] }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6>% Change</h6>
                                </div>
                                <div class="col-sm-6">
                                    {{ $data['percentage_diff'] }}%
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6>Max Price</h6>
                                </div>
                                <div class="col-sm-6">
                                    £{{ $data['max_value'] }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6>Min Price</h6>
                                </div>
                                <div class="col-sm-6">
                                    £{{ $data['min_value'] }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6>Mean Average</h6>
                                </div>
                                <div class="col-sm-6">
                                    £{{ $data['mean_value'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">Alerts</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5>Price</h5>
                                </div>
                                <div class="col">
                                    <h5>Type</h5>
                                </div>
                                <div class="col">
                                    <h5>Date</h5>
                                </div>
                            </div>
                            @foreach ($data['alerts'] as $alert)
                            <div class="row">
                                <div class="col">
                                    {{ $alert->value }}
                                </div>
                                <div class="col">
                                    @if ($alert->type)
                                        Below
                                    @else
                                        Above
                                    @endif
                                </div>
                                <div class="col">
                                    {{ $alert->created_at }}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var coin = {!! $json_data !!};
    </script>
    @push('scripts')
        <script src="/js/currencies/view.js"></script>
    @endpush

</x-app-layout>