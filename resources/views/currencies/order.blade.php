<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('Currencies Order') }}</h2>
    </x-slot>
    <div class="containers grid place-items-center">
        @foreach($data as $currency)
            <div id="{{ $currency['id'] }}" draggable="true" class="draggable grid grid-cols-12 bg-white m-1 h-10 w-1/3 rounded border border-gray-600">
                <div class="col-span-2 justify-self-end m-1.5">
                    <p class="text-lg font-bold">{{ $currency['acronym'] }}</p>
                </div>
                <div class="col-span-1 justify-self-center m-2">
                    @if($currency['logo'])
                        <img id="order_currency_logo" class="w-6 h-6" src={{ $currency['logo'] }}>
                    @endif
                </div>
                <div class="col-span-8 m-1.5">
                    <p class="text-lg font-bold">{{ $currency['name'] }}</p>
                </div>
                <div class="col-span-1 justify-self-end m-1.5">
                    @if($currency['hidden'])
                        <input onclick="hideCurrency({{ $currency }})" type="checkbox" checked>
                    @else
                        <input onclick="hideCurrency({{ $currency }})" type="checkbox">
                    @endif
                </div>
            </div>
        @endforeach
    </div>

    @push('scripts')
        <script src="/js/currencies\order.js"></script>
    @endpush

</x-app-layout>