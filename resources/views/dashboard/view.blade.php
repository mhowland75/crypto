<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Currencies') }}
        </h2>
    </x-slot>

    <div class="row">
            @foreach($data as $currency)
                <div style="padding-top:20px;" class="col-sm-4">
                    <div class="card">
                    <div class="card-header">
                        <a href="/currencies/{{ $currency['acronym'] }}">
                            <div class="row">
                                <div class="col-sm-6"><h4>{{ $currency['currency'] }}</h4></div>
                                <div class="col-sm-6"><h4>{{ $currency['acronym'] }}</h4></div>
                            </div>
                            
                        </a>
                    </div>
                        <div class="card-body">
                            <div style="height:300px;" id={{ $currency['acronym'] }}>
                                <div style="padding-top:25%; padding-left:50%">
                                    <div class="spinner-border"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-sm-6">Price</div>
                                <div class="col-sm-6">£{{ $currency['price'] }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">Percentage Difference</div>
                                <div class="col-sm-6">{{ $currency['percentage_diff'] }}%</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">Max Value</div>
                                <div class="col-sm-6">£{{ $currency['max_value'] }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">Min Value</div>
                                <div class="col-sm-6">£{{ $currency['min_value'] }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">Mean Value</div>
                                <div class="col-sm-6">£{{ $currency['mean_value'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
    </div>

    <script>
        var json_data = {!! $json_data !!};
    </script>
    @push('scripts')
        <script src="js/dashboard2.js"></script>
    @endpush

</x-app-layout>