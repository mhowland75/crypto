<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Global Settings
        </h2>
    </x-slot>

    <form action="/backend/settings/update" method="POST">
        @csrf
        @if ($settings->store_prices)
            <input type="checkbox" id="store_prices" name="store_prices" value="1" checked>
        @else
            <input type="checkbox" id="store_prices" name="store_prices" value="1">
        @endif
        <label for="store_prices"> Store Prices</label><br>
        @if ($settings->preset_price_alerts)
            <input type="checkbox" id="perset_price_alerts" name="perset_price_alerts" value="1" checked>
        @else
            <input type="checkbox" id="perset_price_alerts" name="perset_price_alerts" value="1">
        @endif
        <label for="perset_price_alerts"> Perset price alerts</label><br>
        <input type="submit" value="Submit">
    </form>
</x-app-layout>