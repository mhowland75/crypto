<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\UserPriceAlert;
use App\Models\UserCurrencyOrder;
use Carbon\Carbon;
use App\Services\Datetime;
use App\Services\Graph;
use App\Services\Maths;
use Illuminate\Support\Facades\Http;
use App\Models\Price;
use Auth;
use App\Traits\Data;

class CurrenciesController extends Controller
{
    use Data;
    /**
     * Returns view for selected curreny.
     */
     public function view(Request $request)
     {
        $sortby = collect(['time' => $request->time]);
        $data = Data::buildStats($sortby, $request->currency);
 
        return view('currencies.view', [
            'time_per' => $request->time,
            'data' => $data[0],
            'json_data' => json_encode($data[0]),
        ]);
    }

    /**
     * Currencies index
     */
    public function index(Http $responce, Request $request)
    {
        $request = $request->all();
        $sort = Data::parameaterSort($request);
        $data = Data::buildStats($sort);
        $data = Data::sort($data, $sort);

       return view('currencies.index', [
           'sortby' => $sort,
           'data' => $data,
           'json_data' => json_encode($data)
       ]);
    }

    /**
     * returns the orders page.
     */
    public function order()
    {

        $orderData = Data::buildOrderData();
        //dd($orderData);

        return view ('currencies.order', [
            'data' => $orderData,
        ]);
    }

    /**
     * Ajax endpoint for updating the order
     */
    public function updateOrder(Request $request)
    {
        Data::updateUserCurrenciesOrder($request->all());   

        return json_encode('succsess');
    }

    /**
     * Ajax endpoint for hidding a currency
     */
    public function orderHide(Request $request)
    {
        $status = Data::hideOrder($request);

        return json_encode('succsess');
    }

    /**
     * ajax data enpoint
     */
    public function ajaxData(Http $responce, Request $request)
    {
        $request = $request->all();

        if(isset($request['time']) && isset($request['sortby'])) {
            $time = $request['time'];
            $sortby = $request['sortby'];
        }
        if(!isset($request['time']) && isset($request['sortby'])) {
            $time = '1d';
            $sortby = $request['sortby'];
        }
        if(isset($request['time']) && !isset($request['sortby'])) {
            $time = $request['time'];
            $sortby = 'Top';
        }
        if(!isset($request['time']) && !isset($request['sortby'])) {
            $time = '1d';
            $sortby = 'Top';
        }
        $sortby = collect(['time' => $time, 'type' => $sortby]);
        
        if(!$sortby['time']){
            $sortby['time'] = '1d';
        }

        $data = Data::buildStats($sortby);
        $data = Data::sort($data, $sortby);


        return json_encode($data);
    }
}
