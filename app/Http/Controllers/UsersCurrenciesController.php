<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\UserPriceAlert;
use Carbon\Carbon;
use App\Services\Datetime;
use App\Services\Graph;
use App\Services\Maths;

class UsersCurrenciesController extends Controller
{
     /**
      * Creates user currency
      */
    public function create()
    {
        return view('currencies.user.create', [
            'data' => $data,
            'json_data' => json_encode($data),
        ]);
    }
}
