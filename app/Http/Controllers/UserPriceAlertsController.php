<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserPriceAlert as Alert;
use Auth;
use App\Models\Currency;

class UserPriceAlertsController extends Controller
{
    /**
     * View alert
     */
    public function view()
    {
        return view('alerts.view');
    }

    /**
     * Index alert 
     */
    public function index()
    {
        $alerts = Alert::with('currency')
        ->where('user_id', Auth::id())
        ->where('executed', 0)
        ->get();
        //dd($alerts);

        return view('alerts.index', [
            'alerts' => $alerts
        ]);
    }

    /**
     * Create alert
     */
    public function create(Request $request)
    {
        $currencies = Currency::all();
        return view('alerts.create', [
            'currencies' => $currencies,
        ]);
    }

    /**
     * Stores Price alert
     */
    public function store(Request $request)
    {
        if(isset($request->price) && isset($request->type))
        {
            $re = $request->all();
            //dd( $request->all());
            $alert = new Alert;
            $alert->user_id = Auth::id();
            $alert->currency_id = $request->currency;
            $alert->value = $request->price;
            $alert->type = $request->type;
            $alert->executed = 0;
            // Todo: Form needs to pass experation date
            $alert->expiration_date = null;
            $alert->save();
        }

        return redirect()->back();
    }

    /**
     * returnds the alert edit page
     */
    public function edit(Request $request)
    {
        dd($request->all());
        $alert = [];
        return view('alerts.edit', [
            'alert' => $alert,
        ]);
    }

    public function ajaxCreate(Request $request)
    {
        $currency = Currency::with('prices')->find($request->selected_currency);
        $collection = collect([
            'currency'  => $currency,
            'price'     => $currency->prices->last()->price,
        ]);
        
        //dd($currency->prices->last()->price);

        return json_encode($collection);
    }
}
