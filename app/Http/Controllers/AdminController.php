<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function view()
    {
        $settings = DB::table('global_settings')->get()->first();
        //dd($settings);

        return view('admin.settings', [
            'settings' => $settings
        ]);
    }

    public function update(Request $request)
    {
        if(isset($request->perset_price_alerts)){
            $perset_price_alerts = 1;
        } else {
            $perset_price_alerts = 0;
        }
        if(isset($request->store_prices)){
            $store_prices = 1;
        } else {
            $store_prices = 0;
        }
        DB::table('global_settings')
            ->where('id', 1)
            ->update(['store_prices' => $store_prices, 'preset_price_alerts' => $perset_price_alerts]);
        
        return redirect()->back();
    }
}
