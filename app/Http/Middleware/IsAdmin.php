<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::User();
        if($user->exists()){
            $admin = $user->admin()->get();
            if($admin->isNotEmpty()){
                return $next($request);
            } 
        }

        return redirect('/');
        
    }
}
