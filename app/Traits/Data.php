<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\UserPriceAlert;
use App\Models\UserCurrencyOrder;
use Carbon\Carbon;
use App\Services\Datetime;
use App\Services\Graph;
use App\Services\Maths;
use Illuminate\Support\Facades\Http;
use App\Models\Price;
use Auth;

trait data
{
    /**
     * returns a redused set of data to improve load times
     */
   public static function recoredReduction($recoreds)
   {
       $modus = false;
       $recoredCount = count($recoreds);
       if($recoredCount > 1000000){
           $modus = 400;
       }
       if($recoredCount > 100000 && $recoredCount < 1000000){
           $modus = 50;
       }
       if($recoredCount > 10000 && $recoredCount < 100000){
           $modus = 40;
       }
       if($recoredCount > 1000 && $recoredCount < 10000){
           $modus = 4;
       }
       if(!$modus){
            return $recoreds;
       }
       $result = collect();
       $i = 0;
       foreach($recoreds as $value) {
           if ($i++ % $modus == 0) {
               $result[] = $value;
           }
       }
       //dd($result);
       return $result;
   }
    /**
     * Ajax endpoint for hidding a currency
     */
    public static function orderHide(Request $request)
    {
        $currency = UserCurrencyOrder::where('user_id', Auth::id())->where('currency_id', $request->currency_id)->first();
        if($currency->hidden){
            $d = $currency->update(['hidden' => 0]);
        } else {
            $d = $currency->update(['hidden' => 1]);
        }

        return json_encode('sucsess');
    }

    /**
     * returns a coolection of currencys and all the statstical data.
     */
    public static function buildStats($sortby, $currency = false)
    {
        $time_per = Datetime::calcToFrom($sortby['time']);
        if($currency){
             $currencies = Currency::where('acronym', $currency)->get();
             $prices = $currencies->first()->prices->whereBetween('created_at', [$time_per['from'], $time_per['to']]);
        } else {
             $currencies = Currency::with('UserCurrencyOrder')->get();
             $prices = Price::whereBetween('created_at', [$time_per['from'], $time_per['to']])->get();
        }

        $collection = collect();
        $recoreds = self::recoredReduction($prices);
        foreach($currencies as $key => $currency)
        {
            $prices = $recoreds->where('currency_id', $currency->id);
            if ($prices->isEmpty()) {
                continue;
            }
            $currency_prices = $prices->pluck('price')->toArray();
            $graph_data = self::buildGraphData($prices);
            
            $x = collect([
                'currency' => $currency,
                'acronym'  => $currency->acronym,
                'price'    => self::priceFormat($prices->last()),
                'max_value'=> Maths::max($currency_prices),
                'min_value'=> Maths::min($currency_prices),
                'mean_value'=> Maths::mean($currency_prices),
                'percentage_diff'=> Maths::percentageDiff($prices->first()->price, $prices->last()->price),
                'graph_data' => $graph_data,
                'alerts' => $currency->userPriceAlerts($currency),
                'position' => $currency->position(),
                'hidden' => $currency->UserCurrencyOrder()->first()->hidden,
                'sortby' => $sortby,
            ]);
            $collection->push($x);

        }

        return $collection;
    }

    /**
     * refactor build stats and move build graph data to function
     */
    public static function buildGraphData($prices) 
    {
        $graph_data = [['Date', 'Price']];
        foreach($prices as $price){
            $fprice = self::priceFormat($price);
            $graph_data[] = [$price->created_at->format('m/d H:i'), $fprice];
        }

        return $graph_data;
    }

    /**
     * Returns a formated price
     */
    public static function priceFormat($price = null)
    {
        if ($price->price) {
            $price_array = str_split($price->price);
            $price = (float)$price->price;
            if($price_array[1] == '.' && $price_array[0] == '0'){
                return round($price, 6);
            } else {
                return round($price, 2);
            }
        }
    }

    /**
     * sorts the order of currencys depending on the useres input
     */
    public static function sort($data, $sort_type = false)
    {
        $data = $data->sortBy('position')->values();
        if($sort_type == 'Gainers'){
            $data = $data->sortByDesc('percentage_diff')->values();
        }
        if($sort_type == 'Losers'){
            $data = $data->sortBy('percentage_diff')->values();
        }

        return $data;
    }

    public static function parameaterSort(array $request): array
    {
        return [
            'time' => $request['time'] ?? '1d',
            'type' => $request['sortby'] ?? 'Top'
        ];
    }

    /**
     * Inserts the defult currency order into the data and return weather it was successful
     */
    public static function setDefaultCurrencyOrder()
    {
        $c = Currency::all();
        $f = collect();
        foreach($c as $position => $currency){
            $x = collect(['user_id'=> Auth::id(), 'currency_id' => $currency->id, 'position' => $position, 'hidden'=> 0]);
            $f->push($x);
        }

        return $status = UserCurrencyOrder::insert($f->toArray());
    }

    /**
     * Returns a collection of currency data ordered by the position.
     */
    public static function buildOrderData()
    {
        $currencies = userCurrencyOrder::UserOrder();
        if($currencies->isEmpty()){
            self::setDefaultCurrencyOrder();
            $currencies = Currency::all();
        }
        $collection = collect();
        foreach($currencies as $currency){
            $collection->push(collect([
                'id' => $currency->currency->id,
                'name' => $currency->currency->name,
                'acronym' => $currency->currency->acronym,
                'position' =>$currency->position,
                'hidden' => $currency->hidden,
                'logo' => $currency->currency->logo_url
            ]));
        }

        return $collection->sortBy('position')->values();
    }
    /**
     * Updates the users currency order and returns a json responce
     */
    public static function updateUserCurrenciesOrder(array $order)
    {
        $data = collect();
        $recoreds = UserCurrencyOrder::where('user_id', Auth::id())->get();
        
        foreach($order as $position => $currency){
            //dump( $recoreds->where('currency_id', $currency)->first()->hidden);
            $d = collect([
                'user_id'=> Auth::id(), 
                'currency_id' => $currency, 
                'position' => $position,
                'hidden' => $recoreds->where('currency_id', $currency)->first()->hidden,
            ]);
            $data->push($d);
        }

        UserCurrencyOrder::where('user_id', Auth::id())->delete();

        return UserCurrencyOrder::insert($data->toArray());
    }
    /**
     * Hides the users order
     */
    public static function hideOrder($request)
    {
        $currency = UserCurrencyOrder::where('user_id', Auth::id())
        ->where('currency_id', $request->currency_id)
        ->first();

        if($currency->hidden){
            $status = $currency->update(['hidden' => 0]);
        } else {
            $status = $currency->update(['hidden' => 1]);
        }

        return $status;
    }
}