<?php

namespace App\Services;

use Carbon\Carbon;

class Maths
{
    /**
     * Calculates the mean of an array.
     */
    public static function mean(array $data)
    {
        if(is_array($data))
        {
            $mean = array_sum($data) / count($data);

            return number_format($mean, 2);
        }
    }

    /**
     * Returns the percentage diffrence between two numbers.
     */
    public static function percentageDiff($oldFigure, $newFigure)
    {
        $percentage = (1 - $oldFigure / $newFigure) * 100;

        return number_format($percentage, 2);
    }

    /**
     * Returns the min from an array.
     */
    public static function min(array $data)
    {
        return number_format(min($data), 2);
    }

    /**
     * Returns the max from an array.
     */
    public static function max(array $data)
    {
        return number_format(max($data), 2);
    }
}