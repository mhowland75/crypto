<?php

namespace App\Services;

use Carbon\Carbon;

class Datetime
{
    public static function time(string $data){
        //dd(strlen($data));

        if(isset($data)){
            $time = str_split($data);
            if(count($time) >= 2){
                $period = array_pop($time);
                $time[0] = implode($time);
                $time[1] = $period;
            }
            
            $time_per = Carbon::now();

            if($time[1] == 'h'){
                $time = $time_per->subHour($time[0])->format('Y-m-d H:i:s');
            }
            if($time[1] == 'd'){
                $time = $time_per->subDay($time[0])->format('Y-m-d H:i:s');
            }
            if($time[1] == 'w'){
                $time = $time_per->subWeek($time[0])->format('Y-m-d H:i:s');
            }
            if($time[1] == 'm'){
                $time = $time_per->subMonth($time[0])->format('Y-m-d H:i:s');
            }
            if($time[1] == 'y'){
                $time = $time_per->subYear($time[0])->format('Y-m-d H:i:s');
            }

            return $time;

        } else {
            return false;
        }
    }

    public static function calcToFrom($time = '1d')
    {
        
        if($time == null){
            $time = '1d';
        }
        $data = [];
        $data['from'] = self::time($time);
        $data['to'] = Carbon::now()->format('Y-m-d H:i:s');
        $data['parameter'] = $time;

        return $data;
    }
}