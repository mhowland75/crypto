<?php

namespace App\Services;

use Carbon\Carbon;

class Graph
{
    public static function buildData($data, $from, $to)
    {
        $price = $data->whereBetween('created_at', [$from, $to]);
        if (empty($price))
        {
            return false;
        }
        $data = [];
        $data[] = ['Datetime','Price'];
        foreach ($price as $price)
        {
            $data[] = [$price->created_at->format('h:m'), (float)$price->price];
        }

        return $data;
    }
}