<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class UserCurrencyOrder extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'currency_id', 'position', 'hidden'];

    /**
     * Get the order for the users currencies.
     */
    public function currency()
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function scopeUserOrder($query)
    {
        return $query->with('currency')->where('user_id', Auth::id())->get();
    }
}
