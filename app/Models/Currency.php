<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Currency extends Model
{
    use HasFactory;

    /**
     * Get the prices for the currency.
     */
    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    /**
     * Get the order for the users currencies.
     */
    public function userCurrencyOrder()
    {
        return $this->hasMany(UserCurrencyOrder::class)->where('user_id', Auth::id());
    }

    /**
     * Get the order for the users currencies.
     */
    public function position()
    {
        return $this->hasOne(UserCurrencyOrder::class)->where('user_id', Auth::id())->first()->position;
    }

    /**
     * Get the order for the users currencies.
     */
    public function hidden()
    {
        return $this->hasOne(UserCurrencyOrder::class)->where('user_id', Auth::id())->first()->hidden;
    }

    /**
     * Get the alerts for the currency.
     */
    public function userPriceAlerts()
    {
        return $this->hasMany(UserPriceAlert::class)->where('user_id', Auth::id())->where('executed', 0)->get();
    }

    public function scopeCurrency($query, $currency)
    {
        return $query->with('prices')
        ->where('acronym', $currency)
        ->get()
        ->first();
    }

    public function scopePrice($query, $currency)
    {
        $currency = $query->with('prices')
        ->where('acronym', $currency)
        ->get()
        ->first();

        //dd($currency->prices->last());
        return round($currency->prices()->select('price')->get()->first()->price, 2);
    }

    public function scopePricesBetween($query, $currency, $to, $from)
    {
        $currency = $query->with('prices')
        ->where('acronym', $currency)
        ->get()
        ->first();

        if(isset($currency->prices)){
            return $currency->prices->whereBetween('created_at', [$from, $to]);
        } else {
            return null;
        }
    }
}
