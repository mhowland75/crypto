<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPriceAlert extends Model
{
    use HasFactory;
    protected $table = 'users_price_alerts';

    /**
     * Get the order for the users currencies.
     */
    public function currency()
    {
        return $this->hasOne(Currency::class,'id', 'currency_id');
    }
}
