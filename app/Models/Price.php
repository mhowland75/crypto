<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    use HasFactory;

    /**
     * Get the currency for the prices.
     */
     public function currency()
     {
         return $this->belongsTo(currency::class);
     }
}
