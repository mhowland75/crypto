const draggables = document.querySelectorAll('.draggable')
const containers = document.querySelectorAll('.containers')

draggables.forEach(draggable => {
    draggable.addEventListener('dragstart', () => {
        draggable.classList.add('dragging')
    })

    draggable.addEventListener('dragend', () => {
        draggable.classList.remove('dragging')
        updateOrder()
    })
})

containers.forEach(container => {
    container.addEventListener('dragover', e => {
        e.preventDefault()
        const afterElement = getDragAfterElement(container, e.clientY)
        const draggable = document.querySelector('.dragging')
        if(afterElement == null){
            container.appendChild(draggable)
        } else {
            container.insertBefore(draggable, afterElement)
        }
    })
})

function getDragAfterElement(container, y){
    const draggableElements = [...container.querySelectorAll('.draggable:not(.dagging)')]

    return draggableElements.reduce((closest, child) => {
        const box = child.getBoundingClientRect()
        const offset = y - box.top - box.height / 2
        if (offset < 0 && offset > closest.offset) {
            return { offset: offset, element:child}
        } else {
            return closest
        }
    }, { offset: Number.NEGATIVE_INFINITY }).element
}

function updateOrder() {
    const order = document.querySelectorAll('.draggable')
    order_data = {}
    i = 0
    order.forEach(x => {
        order_data[i] = x.getAttribute('id')
        i++
    })
    $.ajax({
        type:'POST',
        url:'/currencies/user/order/update',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data : order_data,
        success:function(data){
           console.log(data);
        }
     });
}

function hideCurrency(currency_id){
    $.ajax({
        type:'POST',
        url:'/currencies/user/order/hide',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data : { 'currency_id': currency_id},
        success:function(data){
           console.log(data);
        }
     });
}