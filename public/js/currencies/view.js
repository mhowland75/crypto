
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(function () {
    console.log(coin['graph_data']);
    var data = google.visualization.arrayToDataTable(coin['graph_data']);

    var options = {
        curveType: 'function',
        legend: { position: 'bottom' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('graph'));

    chart.draw(data, options);
});
