/**
 * Builds charts using google api
 */
function chart()
{
    google.charts.load('current', {'packages':['corechart']});
    json_data.forEach(function (coin) {
        google.charts.setOnLoadCallback(function () {
            var data = google.visualization.arrayToDataTable(coin['graph_data']);
        
            var options = {
                curveType: 'function',
                legend: { position: 'bottom' },
                hAxis:{
                    baselineColor: '#fff',
                    gridlineColor: '#fff',
                    textPosition: 'none'
                }
            };
        
            var chart = new google.visualization.LineChart(document.getElementById(coin['acronym']));
        
            chart.draw(data, options);
        });
    })
}
/**
 * Updates sortby 
 */
function setSortBy()
{
    document.getElementById(json_data[0]['sortby']['type']).selected = true
}

/**
 * Sets styles on percentage diffrence text 
 */
function setPerDiffstyle()
{
    const prices = document.querySelectorAll('#price_diff')
    prices.forEach(price => {
        percentage = price.innerHTML
        if(percentage[0] == '-'){
            price.classList.add('neg_price')
        } else{
            price.classList.add('pos_price')
        }
    })
}

function updateData()
{
        console.log(123);
}

function main()
{
    
    setPerDiffstyle();
    setSortBy()
    chart()
    
}

main()

setInterval(function(){ 
    $.ajax({
        type:'POST',
        url:'/currencies/ajax/data',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        
        // todo this needs to send back bort information from the page
        data : {
            'sortby': document.getElementById("sortby").value,
            'time': document.getElementById("time_per").value
        },
        success:function(data){
            data = JSON.parse(data);
            data.forEach(currency => {
                currency_id = currency['currency']['id']
                prices = document.getElementById('price-' + currency_id)
                max = document.getElementById('max-' + currency_id)
                min = document.getElementById('min-' + currency_id)
                mean = document.getElementById('mean-' + currency_id)
                perdiff = document.getElementById('perdiff-' + currency_id)
                if(prices != null){
                    prices.innerHTML = '£' + currency['price']
                    max.innerHTML =  '£' + currency['max_value']
                    min.innerHTML =  '£' + currency['min_value']
                    mean.innerHTML =  '£' + currency['mean_value']
                    perdiff.innerHTML =  currency['percentage_diff'] + '%'
                }
            })
        }
     });   
}, 5000);