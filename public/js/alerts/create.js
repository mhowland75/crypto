
function loadCurrencyData()
{
    $.ajax({
        type:'POST',
        url:'/alerts/ajax/create',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data : {
            'selected_currency': $("#selected_currency :selected").val(),
        },
        success:function(data){
            data = JSON.parse(data);
            console.log(data.currency.name);
            $("#name").text(data.currency.name)
            $("#price").text(data.price)
            $("#value").val(data.price)
            $("#coin-image").attr("src",data.currency.logo_url);
        }
     });  
}
$("#selected_currency").change(function(){
    loadCurrencyData()
});

$( document ).ready(function() {
    loadCurrencyData()
});