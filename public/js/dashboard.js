
google.charts.load('current', {'packages':['corechart']});
console.log(graph)
graph.forEach(function (coin) {
    google.charts.setOnLoadCallback(function () {
        var data = google.visualization.arrayToDataTable(coin['graph']);
    
        var options = {
            title: coin['currency'],
            curveType: 'function',
            legend: { position: 'bottom' }
        };
    
        var chart = new google.visualization.LineChart(document.getElementById(coin['currency']));
    
        chart.draw(data, options);
    });
})

