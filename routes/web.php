<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CurrenciesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserPriceAlertsController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/user/settings', [UserSettings::class, 'view'])->middleware(['auth'])->name('user_settings');
Route::get('/currencies/{currency}', [CurrenciesController::class, 'view'])->middleware(['auth'])->name('currencies');
Route::get('/', [CurrenciesController::class, 'index'])->middleware(['auth'])->name('dashboard');
Route::get('/currencies/user/currencies/order', [CurrenciesController::class, 'order'])->middleware(['auth'])->name('dashboard_settings');

// AJAX Endpoints
Route::post('/currencies/user/order/update', [CurrenciesController::class, 'updateOrder'])->middleware(['auth']);
Route::post('/currencies/user/order/hide', [CurrenciesController::class, 'orderHide'])->middleware(['auth']);
Route::post('/currencies/ajax/data', [CurrenciesController::class, 'ajaxData'])->middleware(['auth']);


Route::post('/alerts/store', [UserPriceAlertsController::class, 'store'])->middleware(['auth']);
Route::post('/alerts/update', [UserPriceAlertsController::class, 'update'])->middleware(['auth']);
Route::get('/alerts/view', [UserPriceAlertsController::class, 'view'])->middleware(['auth']);
Route::get('/alerts/index', [UserPriceAlertsController::class, 'index'])->middleware(['auth'])->name('alerts');
Route::get('/alerts/create', [UserPriceAlertsController::class, 'create'])->middleware(['auth'])->name('create-alert');
Route::get('/alerts/edit/{id}', [UserPriceAlertsController::class, 'edit'])->middleware(['auth'])->name('edit-alert');
Route::post('/alerts/ajax/create', [UserPriceAlertsController::class, 'ajaxCreate'])->middleware(['auth']);

Route::middleware([isAdmin::class])->group(function () {
    Route::get('/backend/settings', [AdminController::class, 'view']);
    Route::post('/backend/settings/update', [AdminController::class, 'update']);
});



require __DIR__.'/auth.php';
