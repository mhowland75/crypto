<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CurrancyPriceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('acronym');
            $table->string('logo_url');
            $table->timestamps();
        });

        $currencies = 'BTC,ETH,XRP,LTC,LINK,BCH,XLM,EOS,WBTC,TRX,XTZ,DAI,ATOM,UNI,YFI,COMP,ETC,MKR,OMG,UMA,ALGO,BAT,ZRX,LRC,CGLD,KNC,REP,BAND,NMR,OXT,BAL,NU';
        $url = "https://api.nomics.com/v1/currencies/ticker?key=fe3931329c9d81d7e4549fec93b1f260&ids=" . $currencies . "&convert=GBP";
        $response = Http::get($url);
        $data = $response->json();
        
        $array = [];
        foreach($data as $coin){
            $array[] = ['name' => $coin['name'], 'acronym' => $coin['currency'], 'logo_url' => $coin['logo_url']];
        }

        DB::table('currencies')->insert($array);

        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->integer('currency_id');
            $table->decimal('price', $precision = 8, $scale = 2);
            //$table->datetime('price_date');
            $table->datetime('price_timestamp');
            // $table->string('circulating_supply');
            // $table->float('market_cap');
            // $table->float('transparent_market_cap');
            // $table->integer('num_exchanges');
            // $table->integer('num_pairs');
            // $table->integer('num_pairs_unmapped');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('prices');
    }
}
